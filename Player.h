#pragma once
#include "AnimatingObject.h"
class Player :
    public AnimatingObject
{
public:
    Player(sf::Vector2u Screensize);
    void Input();
    void Update(sf::Time FrameTime);
    void handleSolidCollision(sf::FloatRect otherHitbox);

private:
    sf::Vector2f Velocity;
    float speed;
    float gravity;
    sf::Vector2f previousPos;
    const float jumpValue;
};
