#pragma once
#include <SFML/Graphics.hpp>
#include "Player.h"
#include "Platform.h"
class Game;
class LevelClass
{
public:

	LevelClass(Game* newGamePointer);
	void input();
	void update(sf::Time gameTime);
	void drawTo(sf::RenderTarget& Target);
private:
	Player playerInstance;
	std::vector<Platform> platformInstances;
	Game* gamePointer;
	sf::View camera;
	float platformGap;
	float platformGapIncrease;
	float highestPlatform;
	float platformBuffer;
	void addPlatform();
};

