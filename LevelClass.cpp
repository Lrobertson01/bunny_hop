#include "LevelClass.h"
#include "Game.h"
#include <stdlib.h>
#include <time.h>

LevelClass::LevelClass(Game* newGamePointer)
    : playerInstance(newGamePointer->windowGet().getSize())
    , gamePointer(newGamePointer)
    , platformInstances()
    , camera(newGamePointer->windowGet().getDefaultView())
    , platformGap(50)
    , platformGapIncrease(3)
    , highestPlatform(0)
    , platformBuffer(100)
{

    platformInstances.push_back(Platform());

    sf::Vector2f newPosition;
    newPosition.x = ((float)newGamePointer->windowGet().getSize().x )/ 2.0f;
    newPosition.y = ((float)newGamePointer->windowGet().getSize().y) / 2.0f;

    newPosition.y -= platformInstances[0].getHitbox().height / 2;

    const float offset = 250;

    newPosition.y += offset;

    platformInstances[0].setPosition(newPosition);

    highestPlatform = newPosition.y;

    float cameraTop = camera.getCenter().y - camera.getSize().y / 2.0f;

    while (highestPlatform > cameraTop - platformBuffer)
    {
        addPlatform();
    }
}

void LevelClass::input()
{
    playerInstance.Input();
}

void LevelClass::update(sf::Time GameTime)
{
    playerInstance.Update(GameTime);
    float cameraTop = camera.getCenter().y - camera.getSize().y / 2.0f;

    if (highestPlatform > cameraTop - platformBuffer)
    {
        addPlatform();
    }
    for (int i = 0; i < platformInstances.size(); i++)
    {
        playerInstance.handleSolidCollision(platformInstances[i].getHitbox());
        platformGap = platformGap;
    }
    if (platformInstances[0].getHitbox().top > camera.getCenter().y + camera.getSize().y/2) 
    {
        platformInstances.erase(platformInstances.begin());
    }
}

void LevelClass::drawTo(sf::RenderTarget& Target)
{    
    sf::Vector2f currentViewCentre = camera.getCenter();
    float playerCenterY = playerInstance.getHitbox().top + playerInstance.getHitbox().height / 2;

    if (playerCenterY < currentViewCentre.y)
    {
        camera.setCenter(currentViewCentre.x, playerCenterY);
    }

    Target.setView(camera);
    
    playerInstance.DrawTo(Target);
    for (int i = 0; i < platformInstances.size(); i++)
    {
        platformInstances[i].DrawTo(Target);
    }

    Target.setView(Target.getDefaultView());
}

void LevelClass::addPlatform()
{
    Platform newPlatform;
    sf::Vector2f newPosition;
    newPosition.y = highestPlatform - platformGap;

    int playAreaWidth = 800;
    int minX = camera.getCenter().x - playAreaWidth / 2;
    int maxX = camera.getCenter().x + playAreaWidth / 2;

    maxX -= newPlatform.getHitbox().width;
    newPosition.x = rand() % (minX - maxX) + minX;
    newPlatform.setPosition(newPosition);
    platformInstances.push_back(newPlatform);
    //Make sure the platform gap will never be higher than the player can jump. The actual number of how high they can jump is 222, but i want to give some breathing room
    if (platformGap < 210) 
    {
        platformGap += platformGapIncrease;
    }
    
    highestPlatform = newPosition.y;
}
