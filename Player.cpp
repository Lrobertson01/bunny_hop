#include "Player.h"
#include "AssetManager.h"

Player::Player(sf::Vector2u screensize)
	:AnimatingObject(AssetManager::RequestTexture("Assets/Graphics/PlayerAnimation.png"), 75, 100, 8.0f)
	, Velocity(0.0f, 0.0f)
	, speed (300.0f)
	, gravity (800.0f)
	, jumpValue(-600) 
{
	addClip("Jump", 0, 1);

	PlayClip("Jump", false);

	sf::Vector2f newPosition;
	newPosition.x = ((float)screensize.x - sprite.getGlobalBounds().width) / 2.0f;
	newPosition.y = ((float)screensize.y - sprite.getGlobalBounds().height) / 2.0f;
	sprite.setPosition(newPosition);
}

void Player::Input()
{
	//Give the player a bit of momentum if they stop moving
	if (Velocity.x != 0)
	{
		if (Velocity.x > 0)
		{
			Velocity.x -= speed / 1000;
		}

		if (Velocity.x < 0)
		{
			Velocity.x += speed / 1000;
		}
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A) || sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	{
		Velocity.x = -speed;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D) || sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
		Velocity.x = speed;
	}
}

void Player::Update(sf::Time FrameTime)
{
	previousPos = sprite.getPosition();
	
	sf::Vector2f newPosition = sprite.getPosition() + Velocity * FrameTime.asSeconds();

	sprite.setPosition(newPosition);

	AnimatingObject::Update(FrameTime);

	Velocity.y += gravity * FrameTime.asSeconds();

}

void Player::handleSolidCollision(sf::FloatRect otherHitbox)
{
	if (getHitbox().intersects(otherHitbox))
	{
		float previousBottom = previousPos.y + getHitbox().height;
		float platformTop = otherHitbox.top;
		if (previousBottom < platformTop)
		{
			Velocity.y = jumpValue;
			PlayClip("Jump", false);
		}

	
	}
}
