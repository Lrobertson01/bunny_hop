#include "Game.h"




Game::Game()
	:Window(sf::VideoMode::getDesktopMode(), "Bunny Hop", sf::Style::Titlebar | sf::Style::Close)
	, gameClock()
	, LevelScreenInstance(this)
{
	Window.setMouseCursorVisible(false);
}

void Game::runGameLoop()
{
	while (Window.isOpen())
	{
	Input();
	Update();
	Draw();
	}

}

void Game::Input()
{
	sf::Event event;
	while (Window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			Window.close();
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
		{
			Window.close();
		}

	}
	LevelScreenInstance.input();
}

void Game::Update()
{
	sf::Time frameTime = gameClock.restart();
	LevelScreenInstance.update(frameTime);

}

void Game::Draw()
{
	Window.clear();
	LevelScreenInstance.drawTo(Window);
	Window.display();
}

sf::RenderWindow& Game::windowGet()
{
	return Window;
}
