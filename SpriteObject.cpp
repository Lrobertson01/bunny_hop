#include "SpriteObject.h"

SpriteObject::SpriteObject(sf::Texture& newTexture)
	:sprite(newTexture)
	, speed(200.0f)
{
}

void SpriteObject::DrawTo(sf::RenderTarget& target)
{
	target.draw(sprite);
}

sf::FloatRect SpriteObject::getHitbox()
{
	return sf::FloatRect(sprite.getGlobalBounds());
}

sf::Vector2f SpriteObject::CalculateCollisionDepth(sf::FloatRect otherHitbox)
{
	sf::FloatRect thisHitbox = getHitbox();
	sf::Vector2f centreOther(otherHitbox.left + otherHitbox.width / 2.0f, otherHitbox.top + otherHitbox.height / 2.0f);
	sf::Vector2f centreSprite(thisHitbox.left + thisHitbox.width / 2.0f, thisHitbox.top + thisHitbox.height / 2.0f);

	float distanceX = centreOther.x - centreSprite.x;
	float distanceY = centreOther.y - centreSprite.y;

	float minDistanceX = otherHitbox.width / 2.0f + thisHitbox.width / 2.0f;
	float minDistanceY = otherHitbox.height / 2.0f + thisHitbox.height / 2.0f;

	if (distanceX < 0)
	{
		minDistanceX = -minDistanceX;
	}

	if (distanceY < 0)
	{
		minDistanceY = -minDistanceY;
	}

	float depthX = minDistanceX - distanceX;
	float depthY = minDistanceY - distanceY;

	return sf::Vector2f(depthX, depthY);
}
